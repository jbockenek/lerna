#include <stdint.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <api/api.hpp>
#include "stm/txthread.hpp"
#include "stm/config.h"

#define ADAPTIVE
#define ADAPTIVE_WINDOW 3
#define DEFAULT_ALGO		"NOrec"  // "LLT", "OrecEager", "OrecLazy", "RingSW"
#define DEFAULT_WORKERS		1
#define BATCH_PER_WORKER	200
#define STICKY  			true

#define CFENCE              __asm__ volatile ("":::"memory")

extern void rand_init();
extern void rand_thread_init();

struct padding{
	volatile uint64_t v1;
	uint64_t v2;
	uint64_t v3;
	uint64_t v4;
};
static struct padding *lernaruntime_threadsOrdering;
static int lernaruntime_batchExit;
static int lernaruntime_jobsCount;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ T H R E A D P O O L ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

typedef struct {
    int (*function)(void *);
    void *argument;
} threadpool_task_t;


struct thread_t{
  pthread_t thread;
  bool killed;
  float throughput;
};

static int windowCenter;
static int threadpool_workersCount;
static int threadpool_activeWorkersCount;
static pthread_barrier_t threadpool_barrier;
static struct thread_t *threadpool_threads;
static threadpool_task_t *threadpool_queue;
static int threadpool_queueSize;
static int threadpool_maxQueueSize;
static pthread_key_t threadpool_currentThreadId;

static int debug_batches = 0;
static int debug_epoch = 0;

static void threadpool_memory_violation_handler(int sigNumber) {
	DEBUG("Thread 0x%.8x Handled floating point failure SIGFPE (signal " << sigNumber << ")\n");
	DEBUG("Abort\n");
	stm::TxThread* tx = stm::Self;
	tx->completed = true;
	tx->tmabort(stm::Self);
	DEBUG("Abort done\n");
	pthread_t me = pthread_self();
	for(int w=0; w<threadpool_workersCount; w++){
		if(me==threadpool_threads[w].thread) {
			DEBUG("Kill thread at " << w << "\n");
			threadpool_threads[w].killed = true;
			break;
		}
	}
	DEBUG( "Ping Master!\n");
	pthread_barrier_wait (&(threadpool_barrier));
	DEBUG( "Ping Master Done!\n");
	pthread_exit(&sigNumber);
}

static void threadpool_exit_handler(int sigNumber) {
	DEBUG("Thread 0x%.8x Exit (signal " << sigNumber << ")\n");
	exit(1);
}

static TM_INLINE int threadpool_currentId(){
	return *(int*)pthread_getspecific(threadpool_currentThreadId);
}

void *threadpool_work(void *args){
	pthread_setspecific(threadpool_currentThreadId, args);
	volatile int currentWorkerId = *(int*)args;
	if(STICKY){
		int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		int core_id = (currentWorkerId+1)%num_cores;
		CPU_SET(core_id, &cpuset);
		pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
	}
	stm::thread_init();
	rand_thread_init();
    for(;;) {
    	DEBUG( "T" << currentWorkerId << ": Wait for jobs\n");
   		pthread_barrier_wait (&(threadpool_barrier));
    	DEBUG( "T" << currentWorkerId << ": Found " << threadpool_queueSize << " jobs\n");
    	if(currentWorkerId < threadpool_activeWorkersCount)
			for(volatile int index=currentWorkerId; index<threadpool_queueSize; index+=threadpool_activeWorkersCount){
				stm::Self->age = index;
				DEBUG("T" << currentWorkerId << " start " << index << "~" << debug_batches << "@" << debug_epoch << "\n");
				int jobExit = (*(threadpool_queue[index].function))(threadpool_queue[index].argument);
				if(lernaruntime_threadsOrdering[currentWorkerId].v1 == 2){
					DEBUG("T" << currentWorkerId << " stopped!\n");
					break;	// don't execute more jobs
				}
				if(jobExit != 1000){
					DEBUG("T" << currentWorkerId << " unexpected exit (" << jobExit << ") at " << index << "\n");
					lernaruntime_batchExit = jobExit;
					lernaruntime_jobsCount += index;
					for(int w=0; w<threadpool_activeWorkersCount; w++){
						DEBUG("T" << currentWorkerId << " stopping " << w << "\n");
						lernaruntime_threadsOrdering[w].v1 = 2;	// stop other workers
					}
					break;
				}else{
					int nextWorkerId = currentWorkerId==threadpool_activeWorkersCount-1 ? 0 : currentWorkerId+1;
					lernaruntime_threadsOrdering[currentWorkerId].v1 = 0;
					lernaruntime_threadsOrdering[nextWorkerId].v1 = 1;
					DEBUG("T" << currentWorkerId << " release T" << nextWorkerId << "\n");
				}
				DEBUG("T" << currentWorkerId << " done " << index << "\n");
			}
    	DEBUG( "T" << currentWorkerId << ": Ping Master!\n");
    	pthread_barrier_wait (&(threadpool_barrier));
    	DEBUG( "T" << currentWorkerId << ": Ping Master Done!\n");
    }
    pthread_exit(NULL);
    return NULL;
}

static TM_INLINE bool createWorker(int i){
	DEBUG("Create worker " << i << "\n");
	threadpool_threads[i].killed = false;
	int rc;
	int* wid = (int*)malloc(sizeof(int));
	*wid = i;
	if((rc = pthread_create(&(threadpool_threads[i].thread), NULL, threadpool_work, (void*)wid)) != 0) {
		DEBUG( "Fail to create thread, error code = " << rc << "\n");
		return false;
	}
	DEBUG("Create worker " << i << " done\n");
	return true;
}

static bool threadpool_create()
{
    threadpool_threads = (struct thread_t *)malloc(sizeof(struct thread_t) * threadpool_workersCount);

    DEBUG( "Init threadpool_barrier " << ( threadpool_workersCount+1 ) << "\n");
    if((pthread_barrier_init(&(threadpool_barrier), NULL, threadpool_workersCount+1) != 0) ||
       ((pthread_key_create(&threadpool_currentThreadId, NULL) != 0))){
    	return false;
    }

   threadpool_queue = (threadpool_task_t *)malloc (sizeof(threadpool_task_t) * threadpool_maxQueueSize);

	DEBUG("Register SIG Handler\n");
	struct sigaction actions;
	memset(&actions, 0, sizeof(actions));
	sigemptyset(&actions.sa_mask);
	actions.sa_flags = 0;
	actions.sa_handler = threadpool_memory_violation_handler;
	sigaction(SIGILL, &actions, NULL); // Illegal Instruction
	sigaction(SIGFPE, &actions, NULL); // Floating point exception
	sigaction(SIGSEGV, &actions, NULL); // Invalid memory reference

	struct sigaction exitAction;
	memset(&exitAction, 0, sizeof(exitAction));
	sigemptyset(&exitAction.sa_mask);
	exitAction.sa_flags = 0;
	exitAction.sa_handler = threadpool_exit_handler;
	sigaction(SIGINT, &exitAction, NULL); // Exit signal

    /* Start worker threadpool_threads */
   for(int w=0; w<threadpool_workersCount; w++) {
	   if(!createWorker(w)) return false;
	   threadpool_threads[w].throughput = 0;
   }
   DEBUG("Pool created\n");
   return true;
}

static bool threadpool_add(int (*function)(void *), void *argument)
{
	if(threadpool_queueSize==threadpool_maxQueueSize) return true;
    /* Add task to threadpool_queue */
	threadpool_queue[threadpool_queueSize].function = function;
	threadpool_queue[threadpool_queueSize].argument = argument;
	threadpool_queueSize++;
    return false;
}

static TM_INLINE void threadpool_reset()
{
	DEBUG( "Reset Pool\n");
	threadpool_queueSize = 0;
	for(int w=0; w<threadpool_workersCount; w++)
		if(threadpool_threads[w].killed){
			pthread_detach(threadpool_threads[w].thread);
			createWorker(w);
		}
}

static TM_INLINE void threadpool_start()
{
    DEBUG( "Release workers\n");
	pthread_barrier_wait (&(threadpool_barrier));	// release threadpool_workersCount
	DEBUG( "Release workers Done\n");

	DEBUG( "Wait for workers\n");
	pthread_barrier_wait (&(threadpool_barrier));
	DEBUG( "Wait for workers Done\n");
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ R U N T I M E ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

stm::TxThread* lernaruntime_startTx(jmp_buf *_jmpbuf, uint32_t abort_flags){
	DEBUG("Tx started\n");
	stm::TxThread* tx = stm::Self;
	stm::begin(tx, _jmpbuf, abort_flags);
	DEBUG("Tx " << tx << " " << tx->age << " begin "  << _jmpbuf << " " << abort_flags << " \n");
	CFENCE;
	return tx;
}

void lernaruntime_endTx(){
	int currentWorkerId = threadpool_currentId();
	DEBUG("T" << currentWorkerId << " wait\n");
	while(!lernaruntime_threadsOrdering[currentWorkerId].v1);
	stm::TxThread *tx = stm::Self;
	if(lernaruntime_threadsOrdering[currentWorkerId].v1 == 2){
		if(tx->nesting_depth){ // Tx started
			DEBUG("T" << currentWorkerId << " Abort\n");
			tx->completed = true;
			tx->tmabort(tx);
			DEBUG("T" << currentWorkerId << " Abort done\n");
		}
		return;
	}
	if(tx->nesting_depth){ // Tx started
		DEBUG("T" << currentWorkerId << " Commit\n");
		stm::commit(tx);
		DEBUG("T" << currentWorkerId << " Inc Counters\n");
		for(int i = 0; i < tx->i32_counters_size; i++){
			(*tx->i32_counters[i]) += tx->i32_counters_increments[i];
		}
		tx->i32_counters_size = 0;

		for(int i = 0; i < tx->fp_counters_size; i++){
			(*tx->fp_counters[i]) += tx->fp_counters_increments[i];
		}
		tx->fp_counters_size = 0;

		DEBUG("T" << currentWorkerId << " Commit done\n");
	}
}

void lernaruntime_init(){
	char* conf;
	conf = getenv ("STM_CONFIG");
	if(conf==NULL){
		setenv("STM_CONFIG", DEFAULT_ALGO, 1);
	}
    stm::sys_init();
    conf = getenv ("HYDRA_WORKERS");
    threadpool_workersCount = conf==NULL ? DEFAULT_WORKERS : atoi(conf);
#ifdef ADAPTIVE
    threadpool_activeWorkersCount = threadpool_workersCount/2;
#else
    threadpool_activeWorkersCount = threadpool_workersCount;
#endif
    windowCenter = threadpool_activeWorkersCount-1;
    threadpool_maxQueueSize = threadpool_workersCount * BATCH_PER_WORKER;
    printf("Hydra runtime configured using workers == %d\n", threadpool_workersCount);
    rand_init();
    if(!threadpool_create())
		DEBUG("Failed to create threadpool\n");
	lernaruntime_threadsOrdering = (struct padding*)malloc(threadpool_workersCount * sizeof(struct padding));
	lernaruntime_jobsCount = 0;
}


void lernaruntime_destory(){
	stm::sys_shutdown();
}

int lernaruntime_newBatch(int loop){
	DEBUG("Start batch " << ++debug_batches << "@" << debug_epoch << "\n");
	threadpool_queueSize = 0;
	lernaruntime_batchExit = 1000; // assume normal exit
	lernaruntime_threadsOrdering[0].v1 = 1;
	for(int w=1; w<threadpool_workersCount; w++){
		lernaruntime_threadsOrdering[w].v1 = 0;
	}
	threadpool_reset();
	return 1;
}

int lernaruntime_addJob(void* args, int (*work_para)(void*), int (*work_seq)(void*), int nested){
	int full = threadpool_add(threadpool_activeWorkersCount>1 ? work_para : work_para, args);
	DEBUG("Add job " << threadpool_queueSize << " [full=" << full << "]\n");
	return full;
}

int lernaruntime_endBatch(int nested){
#ifdef ADAPTIVE
	struct timeval start, end;
	gettimeofday(&start, NULL);
#endif
	threadpool_start();
#ifdef ADAPTIVE
	if(lernaruntime_batchExit==1000){
		gettimeofday(&end, NULL);
		long time_diff = end.tv_usec - start.tv_usec;
		if(end.tv_sec > start.tv_sec){
			time_diff += (end.tv_sec - start.tv_sec) * 1000000;
		}
		threadpool_threads[threadpool_activeWorkersCount-1].throughput = (float)threadpool_queueSize / time_diff;
		LOG("Throughput " << threadpool_activeWorkersCount << "=" << threadpool_threads[threadpool_activeWorkersCount-1].throughput << "\n");
		int missing_worker = -1;
		for(int i=1; i<=ADAPTIVE_WINDOW; i++){
			if(windowCenter+i<threadpool_workersCount && threadpool_threads[windowCenter+i].throughput==0){
				missing_worker = windowCenter+i;
				break;
			}
			if(windowCenter-i>=0 && threadpool_threads[windowCenter-i].throughput==0){
				missing_worker = windowCenter-i;
				break;
			}
		}
		if(missing_worker<0){
			int max = windowCenter+ADAPTIVE_WINDOW;
			int min = windowCenter-ADAPTIVE_WINDOW;
			if(min<0) min = 0;
			if(max>=threadpool_workersCount) max = threadpool_workersCount-1;
			int best_worker = min;
			for(int i=min+1; i<=max; i++){
				if(threadpool_threads[i].throughput > threadpool_threads[best_worker].throughput){
					best_worker = i;
				}
			}
			windowCenter = best_worker;
			threadpool_activeWorkersCount = best_worker+1;
			LOG("Best is " << threadpool_activeWorkersCount << "\n");
		}else{
			threadpool_activeWorkersCount = missing_worker+1;
			LOG("Try " << threadpool_activeWorkersCount << "\n");
		}
//		printf("%lu;%d;%f\n", (end.tv_sec* 1000000+start.tv_usec), threadpool_activeWorkersCount, (float)threadpool_queueSize / time_diff);
	}else{
		printf("Adaptive Mode: selected workers=%d\n", threadpool_activeWorkersCount);
		threadpool_activeWorkersCount = threadpool_workersCount/2;
		windowCenter = threadpool_activeWorkersCount-1;
	}
#endif
	return lernaruntime_batchExit;
}

int lernaruntime_getJobsCount(){
	int temp;
	if(lernaruntime_batchExit!=1000){
		temp = lernaruntime_jobsCount;	// we increment it with the last index
		lernaruntime_jobsCount = 0; // reset count
	}else{
		lernaruntime_jobsCount += threadpool_queueSize;
		temp = lernaruntime_jobsCount;
	}
	DEBUG("Jobs Count = " << temp << " \n");
	return temp;
}

TM_INLINE void* lernaruntime_readTx_Ptr(void** addr, stm::TxThread* thread){
	DEBUG("Tx" << thread->age << " " << thread <<" ReadPtr " << addr << " \n");
	return stm::DISPATCH<void*, sizeof(void*)>::read(addr, thread);
}

TM_INLINE void lernaruntime_writeTx_Ptr(void** addr, void* val, stm::TxThread* thread)
{
	DEBUG("Tx" << thread->age << " " << thread <<" WritePtr " << addr << "=" << val << " \n");
	stm::DISPATCH<void*, sizeof(void*)>::write(addr, val, thread);
}

TM_INLINE uint8_t lernaruntime_readTx_8bit(uint8_t* addr, stm::TxThread* thread){
	DEBUG("Tx" << thread->age << " " << thread <<" Read8 " << addr << " \n");
	return stm::DISPATCH<uint8_t, sizeof(uint8_t)>::read(addr, thread);
}

TM_INLINE void lernaruntime_writeTx_8bit(uint8_t* addr, uint8_t val, stm::TxThread* thread)
{
	DEBUG("Tx" << thread->age << " " << thread <<" Write8 " << addr << "=" << val << " \n");
	stm::DISPATCH<uint8_t, sizeof(uint8_t)>::write(addr, val, thread);
}

TM_INLINE int lernaruntime_readTx_16bit(uint16_t* addr, stm::TxThread* thread){
	DEBUG("Tx" << thread->age << " " << thread <<" Read16 " << addr << " \n");
//	 return stm::DISPATCH<uint16_t, sizeof(uint16_t)>::read(addr, thread);	// NOT SUPPORTED under Testbed HW
	std::cout << "No Support for 16bit read!\n";
	return 0;
}

TM_INLINE void lernaruntime_writeTx_16bit(uint16_t* addr, uint16_t val, stm::TxThread* thread)
{
	DEBUG("Tx" << thread->age << " " << thread <<" Write16 " << addr << "=" << val << " \n");
//	stm::DISPATCH<uint16_t, sizeof(uint16_t)>::write(addr, val, thread);	// NOT SUPPORTED under Testbed HW
	std::cout << "No Support for 16bit write!\n";
}

TM_INLINE uint32_t lernaruntime_readTx_32bit(uint32_t* addr, stm::TxThread* thread){
	DEBUG("Tx" << thread->age << " " << thread <<" Read32 " << addr << " \n");
	return stm::DISPATCH<uint32_t, sizeof(uint32_t)>::read(addr, thread);
}

TM_INLINE void lernaruntime_writeTx_32bit(uint32_t* addr, uint32_t val, stm::TxThread* thread)
{
	DEBUG("Tx" << thread->age << " " << thread <<" Write32 " << addr << "=" << val << " \n");
	stm::DISPATCH<uint32_t, sizeof(uint32_t)>::write(addr, val, thread);
}

TM_INLINE void lernaruntime_incTx_32bit(uint32_t* addr, uint32_t val)
{
	if (threadpool_activeWorkersCount == 1) {
		*addr += val;
		return;
	}
	stm::TxThread* thread = stm::Self;
	DEBUG("Tx" << thread->age << " " << thread <<" Inc32 " << addr << "=" << val << " \n");
	int index = thread->i32_counters_size++;
	thread->i32_counters[index] = addr;
	thread->i32_counters_increments[index] = val;
}

TM_INLINE uint64_t lernaruntime_readTx_64bit(uint64_t* addr, stm::TxThread* thread){
	uint64_t r = stm::DISPATCH<uint64_t, sizeof(uint64_t)>::read(addr, thread);
	DEBUG("Tx" << thread->age << " " << thread <<" Read64 " << addr << "--" << r << " \n");
	return r;
}

TM_INLINE void lernaruntime_writeTx_64bit(uint64_t* addr, uint64_t val, stm::TxThread* thread){
	DEBUG("Tx" << thread->age << " " << thread <<" Write64 " << addr << "=" << val << " \n");
	stm::DISPATCH<uint64_t, sizeof(uint64_t)>::write(addr, val, thread);
}

TM_INLINE float lernaruntime_readTx_float(float* addr, stm::TxThread* thread)
{
	DEBUG("Tx" << thread->age << " " << thread <<" ReadF " << addr << " \n");
	return stm::DISPATCH<float, sizeof(float)>::read(addr, thread);
}

TM_INLINE void lernaruntime_writeTx_float(float* addr, float val, stm::TxThread* thread)
{
	DEBUG("Tx" << thread->age << " " << thread <<" WriteF " << addr << "=" << val << " \n");
	stm::DISPATCH<float, sizeof(float)>::write(addr, val, thread);
}

TM_INLINE void lernaruntime_incTx_float_float(float* addr, float val)
{
	stm::TxThread* thread = stm::Self;
	DEBUG("Tx" << thread->age << " " << thread <<" IncF " << addr << "=" << val << " \n");
	int index = thread->fp_counters_size++;
	thread->fp_counters[index] = addr;
	thread->fp_counters_increments[index] = val;
}

TM_INLINE void lernaruntime_incTx_float_double(float* addr, double val)
{
	if (threadpool_activeWorkersCount == 1) {
		*addr += val;
		return;
	}
	stm::TxThread* thread = stm::Self;
	DEBUG("Tx" << thread->age << " " << thread <<" IncF " << addr << "=" << val << " \n");
	int index = thread->fp_counters_size++;
	thread->fp_counters[index] = addr;
	thread->fp_counters_increments[index] = (float)val;
}

TM_INLINE double lernaruntime_readTx_double(double* addr, stm::TxThread* thread){
	DEBUG("Tx" << thread->age << " " << thread <<" ReadD " << addr << " \n");
	return stm::DISPATCH<double, sizeof(double)>::read(addr, thread);
}

TM_INLINE void lernaruntime_writeTx_double(double* addr, double val, stm::TxThread* thread)
{
	if (threadpool_activeWorkersCount == 1) {
		*addr += val;
		return;
	}
	DEBUG("Tx" << thread->age << " " << thread <<" WriteD " << addr << "=" << val << " \n");
	stm::DISPATCH<double, sizeof(double)>::write(addr, val, thread);
}
