
#define DEBUG_TYPE "lerna_dictionary"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Pass.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
#include <iostream>
#include <fstream>
using namespace std;
using namespace llvm;
using namespace lerna;

STATISTIC(LernaDictionaryCounter, "Build dictionary of functions that are safe to call within transaction");

#define LOG(X) X

namespace lerna{
  // LernaDictionary - The first implementation, without getAnalysisUsage.
  class LernaDictionary : public FunctionPass, LernaPass {
   public:
    static char ID; // Pass identification, replacement for typeid
    LernaDictionary() : FunctionPass(ID) {}

    virtual bool runOnFunction(Function &function) {
      ++LernaDictionaryCounter;
      LOG(dbgs() << "LernaDictionary: " << function.getName() << '\n');
	  SetVector<BasicBlock*> blocksToCheck;
	  for(ilist<BasicBlock>::iterator sItr = function.getBasicBlockList().begin(), eItr = function.getBasicBlockList().end(); sItr!=eItr; sItr++){
			BasicBlock* bb = &*sItr;
			blocksToCheck.insert(bb);
			DBG(dbgs() << "\t\tAdd block " + bb->getName() + "\n");
	  }
	  LOG(dbgs() << checkInvalidCalls(blocksToCheck) << "\n");
      if(checkInvalidCalls(blocksToCheck)==EXTRACTABLE){
		  dictionary.open("DICTIONARY.txt", ios::out | ios::app | ios::binary);
		  dictionary << function.getName().data() << "\n";
		  dictionary.close();
      }
      return false;
    }
   private:
   	ofstream dictionary;
  };
}

char LernaDictionary::ID = 0;
static RegisterPass<LernaDictionary>
Y("dictionary", "LernaVM: Build dictionary of functions that are safe to call within transaction");

char &lerna::LernaDictionaryID = LernaDictionary::ID;
FunctionPass *createLernaDictionaryPass() { return new LernaDictionary(); }
