/*
 * Grapher.cpp
 *
 *       
 */

#define DEBUG_TYPE "lerna_Grapher"
#include "llvm/Support/Debug.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/ilist.h"
using namespace llvm;
using namespace lerna;

namespace lerna{
class LernaGrapher: public llvm::FunctionPass, public LernaPass{
public:
	static char ID;

	explicit LernaGrapher() :
		FunctionPass(ID){
	}

	bool runOnFunction(Function &F);
};
}

char LernaGrapher::ID = 0;
static RegisterPass<LernaGrapher>
Y("grapher", "LernaVM: View Function Graph");

char &lerna::LernaGrapherID = LernaGrapher::ID;
FunctionPass *createLernaGrapherPass() {
	return new LernaGrapher();
}

bool LernaGrapher::runOnFunction(Function &F) {
	SetVector<BasicBlock*> blocks;
	for(ilist<BasicBlock>::iterator sItr = F.getBasicBlockList().begin(), eItr = F.getBasicBlockList().end(); sItr!=eItr; sItr++){
			BasicBlock* bb = &*sItr;
			blocks.insert(bb);
			DBG(dbgs() << "\t\tAdd block " + bb->getName() + "\n");
	}
	std::string fname = std::string(F.getName().data());
	printGraph(blocks,  fname + ".cfg");
	return false;
}
