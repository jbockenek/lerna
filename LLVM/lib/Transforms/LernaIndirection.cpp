
#define DEBUG_TYPE "lerna_indirection"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Pass.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
using namespace llvm;
using namespace lerna;

STATISTIC(LernaIndirectionCounter, "Transforms usage of any scaler into pointer to the scaler");

#define LOG(X) X

namespace lerna{
  // LernaIndirection - The first implementation, without getAnalysisUsage.
  class LernaIndirection : public FunctionPass {
   public:
    static char ID; // Pass identification, replacement for typeid
    LernaIndirection() : FunctionPass(ID) {}

    virtual bool runOnFunction(Function &function) {
      ++LernaIndirectionCounter;
      LOG(dbgs() << "LernaIndirection: " << function.getName() << '\n');

      Function::BasicBlockListType &code = function.getBasicBlockList();
      for (Function::BasicBlockListType::iterator bs = code.begin(), es = code.end(); bs != es; ++bs) {
		  BasicBlock* bbb = &(*bs);
		  LOG(dbgs() << bbb->getName() << ":\n");
		  for (BasicBlock::iterator I = bbb->begin(), E = bbb->end(); I != E; ++I) {
			  if(AllocaInst* i = dyn_cast<AllocaInst>(I)){
				  if(isa<PointerType>(i->getAllocatedType()))
					  LOG(dbgs() << "Pointer:" << *I << "\n");
				  else{
					  // pointer type of the scaler
					  Type * TyPtr = PointerType::getUnqual(i->getAllocatedType());
					  // allocate pointer variable
					  AllocaInst* AllocaPtr = new AllocaInst(TyPtr, 0, i->getName() + "_ptr", i);
					  LOG(dbgs() << "Scaler:" << *I << "\n");
					  std::vector<User*> Users(I->user_begin(), I->user_end());
					  for (std::vector<User*>::iterator use = Users.begin(), useE = Users.end(); use != useE; ++use){
						  Instruction *inst = cast<Instruction>(*use);
						  // load the pointer value
						  LoadInst* LoadVal = new LoadInst(AllocaPtr, i->getName() + "_load", inst);
						  // replace usage of scaler with its pointer
						  inst->replaceUsesOfWith(i, LoadVal);
					  }
					  // Change variable name
					  i->setName("lerna_" + i->getName() + "_val");
					  // store the address of the pointer variable into the pointer
					  new StoreInst(i, AllocaPtr, i->getNextNode());
				  }
			  }
		  }
      }
      return false;
    }
  };
}

char LernaIndirection::ID = 0;
static RegisterPass<LernaIndirection>
Y("indirection", "LernaVM: Transforms usage of any scaler into pointer to the scaler");

char &lerna::LernaIndirectionID = LernaIndirection::ID;
FunctionPass *createLernaIndirectionPass() { return new LernaIndirection(); }
