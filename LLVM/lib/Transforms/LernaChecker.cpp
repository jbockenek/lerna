/*
 * Checker.cpp
 *
 *       
 */

#define DEBUG_TYPE "lerna_checker"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Dominators.h"
using namespace llvm;
using namespace lerna;

#define LOG(X) X

STATISTIC(NumExtractableLoops, 	"Number of parallizable loops blocks");
STATISTIC(NumSkippedLoops, 		"Number of skipped loops 	[TOTAL]");
STATISTIC(NumComplexLoops, 		"Number of skipped loops	[Complex]");
STATISTIC(NumInvalidIndvarLoops,"Number of skipped loops	[Indvar]");
STATISTIC(NumWrapperLoops,		"Number of skipped loops	[Wrapper]");
STATISTIC(NumAllocaLoops,		"Number of skipped loops	[Alloca]");
STATISTIC(NumPhiLoops,			"Number of skipped loops	[Phi]");
STATISTIC(NumExternalCallLoops,	"Number of skipped loops	[Call]");

namespace lerna{
class LernaChecker: public LernaLoopPass {

private:
	Constant *FuncLernaLog;
	void initLernaCallBacks(Module* module) {
		LLVMContext &Context = module->getContext();
		ArrayRef<Type *> TyFuncLernaLogArgs((Type *) PointerType::getUnqual(IntegerType::getInt8Ty(Context)));
		FunctionType *TyFuncLernaLog = FunctionType::get(Type::getVoidTy(Context), TyFuncLernaLogArgs, false);
		FuncLernaLog = module->getOrInsertFunction("lernavm_log",TyFuncLernaLog);
	}
public:
	static char ID;

	explicit LernaChecker() :
		LernaLoopPass(ID){
	}

	virtual bool runOnLoop(Loop *L, LPPassManager &LPM);

	virtual void getAnalysisUsage(AnalysisUsage &AU) const {
		AU.addRequiredID(BreakCriticalEdgesID);
		AU.addRequiredID(LoopSimplifyID);
		AU.addRequired<DominatorTreeWrapperPass> ();
	}
private:
	void printLoopGraph(llvm::Loop* loop);
};
}

char LernaChecker::ID = 0;
static RegisterPass<LernaChecker>
Y("checker", "LernaVM: Reconstruct loops to run iterations in parallel as functions");

char &lerna::LernaCheckerID = LernaChecker::ID;
LoopPass *createLernaCheckerPass() {
	return new LernaChecker();
}

void LernaChecker::printLoopGraph(llvm::Loop* loop){
	SetVector<BasicBlock*> blocks;
	for (unsigned i = 0; i != loop->getBlocks().size(); ++i) {
		BasicBlock* bb = loop->getBlocks()[i];
		blocks.insert(bb);
		DBG(dbgs() << "\t\tAdd block " + bb->getName() + "\n");
	}
	printGraph(blocks);
}

bool LernaChecker::runOnLoop(Loop *L, LPPassManager &LPM) {
	LOG(dbgs() << "Checker: Processing Loop '" << L->getBlocks().front()->getName() << "' at " << L->getBlocks().front()->getParent()->getName() << "() : Blocks = " << L->getBlocks().size() << "\n");
	DBG(dumb(L->getHeader()->getParent(), ""));
	LOG(printLoopGraph(L));
	int result = checkExtractableLoop(L);
	if(result == EXTRACTABLE){
		NumExtractableLoops++;
		Function* function = L->getHeader()->getParent();
		Module* module = function->getParent();
		initLernaCallBacks(module);
		LLVMContext &Context = module->getContext();

		for (unsigned i = 0; i != L->getBlocks().size(); ++i) {
		   	BasicBlock* bb = L->getBlocks()[i];
			StringRef ss = std::string(bb->getName()) + "@" + std::string(function->getName());
			Constant * tname = ConstantDataArray::getString(Context, ss, true);
			GlobalVariable* TBloc = new GlobalVariable(
					*module,
					tname->getType(),
					true,
					GlobalValue::InternalLinkage,
					tname,
					"__" + bb->getName() + "_" + function->getName());
			Type* TyStruct = PointerType::getUnqual(IntegerType::getInt8Ty(Context));
			Instruction::CastOps OpCastStruct = CastInst::getCastOpcode(TBloc, false, TyStruct, false);
			Instruction *ICastStruct = CastInst::Create(OpCastStruct, TBloc, TyStruct, TBloc->getName(), bb->getTerminator());
			ArrayRef<Value*> ParamFuncLerna(ICastStruct);
			CallInst::Create(FuncLernaLog, ParamFuncLerna, "", bb->getTerminator());
		}
	}else{
		NumSkippedLoops++;
		switch(result){
			case COMPLEX:		NumComplexLoops++; break;
			case REFACTORED:	break;
			case INVALID_INDVAR:NumInvalidIndvarLoops++; break;
			case WRAPPER:		NumWrapperLoops++; break;
			case ALLOCA:		NumAllocaLoops++; break;
			case START_PHI:		NumPhiLoops++; break;
			case EXTERNAL_CALL:	NumExternalCallLoops++; break;
		}
	}
	return false;
}
