#define DEBUG_TYPE "LernaProfiler"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
using namespace llvm;
using namespace lerna;

#define LOG(X) X

namespace lerna{

class LernaProfiler: public BasicBlockPass {

private:
	Constant *FuncLernaLog;
	void initLernaCallBacks(Module* module) {
		LLVMContext &Context = module->getContext();
		ArrayRef<Type *> TyFuncLernaLogArgs((Type *) PointerType::getUnqual(IntegerType::getInt8Ty(Context)));
		FunctionType *TyFuncLernaLog = FunctionType::get(Type::getVoidTy(Context), TyFuncLernaLogArgs, false);
		FuncLernaLog = module->getOrInsertFunction("lernavm_log",TyFuncLernaLog);
	}
public:
	static char ID; // Pass identification, replacement for typeid
	LernaProfiler() :
		BasicBlockPass(ID) {
	}

	virtual bool runOnBasicBlock(BasicBlock &BB) {

		bool refactored = BB.getParent()->getName().startswith("lerna");
		if(refactored){
			  LOG(dbgs() << "Skip: Refactored Function '" << BB.getParent()->getName() << "'\n");
			  return false;
		}

		LOG(dbgs() << "Profiling '" << BB.getName() << "@" << BB.getParent()->getName() << "'\n");

		Module* module = BB.getParent()->getParent();
		initLernaCallBacks(module);
		LLVMContext &Context = module->getContext();
		StringRef ss = std::string(BB.getName()) + "@" + std::string(BB.getParent()->getName());
		Constant * tname = ConstantDataArray::getString(Context, ss, true);
		GlobalVariable* TBloc = new GlobalVariable(
				*module,
				tname->getType(),
				true,
				GlobalValue::InternalLinkage,
				tname,
				"__" + BB.getName() + "_" + BB.getParent()->getName());
		Type* TyStruct = PointerType::getUnqual(IntegerType::getInt8Ty(Context));
		Instruction::CastOps OpCastStruct = CastInst::getCastOpcode(TBloc, false, TyStruct, false);
		Instruction *ICastStruct = CastInst::Create(OpCastStruct, TBloc, TyStruct, TBloc->getName(), BB.getTerminator());
		ArrayRef<Value*> ParamFuncLerna(ICastStruct);
		CallInst::Create(FuncLernaLog, ParamFuncLerna, "", BB.getTerminator());
		return false;
	}
};
}

char LernaProfiler::ID = 0;
static RegisterPass<LernaProfiler>
Y("profiler", "LernaVM: Print basic block name at execution");

char &lerna::LernaProfilerID = LernaProfiler::ID;
BasicBlockPass *createLernaProfilerPass() {
	return new LernaProfiler();
}
