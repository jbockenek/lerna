/*
 * Transactifier.cpp
 */

#define DEBUG_TYPE "lerna_transactifier"
#include "llvm/Support/Debug.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/ilist.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/CallSite.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/IRBuilder.h"

using namespace llvm;
using namespace lerna;
#define DBG(X) X
namespace lerna{

class LernaTransactifier: public llvm::FunctionPass, public LernaPass{
public:
	static char ID;
	Module* M;
	const DataLayout *D;
	Function** func_load;       // 8b, 16b, 16baligned, 32b, 32b aligned, 64b, 64b aligned or U1, U2, U4, U8
	Function** func_store;      // 8b, 16b, 16baligned, 32b, 32b aligned, 64b, 64b aligned or U1, U2, U4, U8

	explicit LernaTransactifier() :
		FunctionPass(ID){
	}

	bool runOnFunction(Function &F);
	Value* replaceLoad(LoadInst* li, Value* tx, std::vector<Value*>* additionalArgs);
	void replaceStore(StoreInst* si, Value* tx, std::vector<Value*>* additionalArgs);
	void initLoadStore(std::vector<Type*>* additionalArgs, const char* nameModifier);
	const Type* getLoadStoreConversion(Module* M, const DataLayout* TD, Type* type);
	Function* getIntLoadStoreFunction(Function** functions, const Type* type, bool aligned);

	void getAnalysisUsage(AnalysisUsage &AU) const
	{
		AU.addRequired<DataLayoutPass> ();
		AU.addRequired<DominatorTreeWrapperPass>();
		AU.addRequired<PostDominatorTree>();
    }
};
}

char LernaTransactifier::ID = 0;
static RegisterPass<LernaTransactifier>
Y("transactifier", "LernaVM: Transactify the code");

char &lerna::LernaTransactifierID = LernaTransactifier::ID;
FunctionPass *createLernaTransactifierPass() {
	return new LernaTransactifier();
}

bool LernaTransactifier::runOnFunction(Function &F) {
	if(!F.getName().startswith("lerna_") || F.getName().startswith("lerna_seq")){
		 return false;
	}
	LOG(dbgs() << "Transactifiy: Processing Function '" << F.getName() << "'\n");
	M = F.getParent();
	DataLayoutPass* DP = &getAnalysis<DataLayoutPass> ();
	D = &DP->getDataLayout();

	/* Initialize Runtime */
	/*
	 * The @llvm.global_ctors array contains a list of constructor functions, priorities,
	 * and an optional associated global or function. The functions referenced by this array
	 * will be called in ascending order of priority (i.e. lowest first) when the module is
	 * loaded. The order of functions with the same priority is not defined.
	 */
	{
		GlobalVariable * global_init = M->getGlobalVariable("llvm.global_ctors");

		std::vector<Constant*> initElems;
		if (global_init) {
			// get other initializers
			ConstantArray* init = cast<ConstantArray>(global_init->getInitializer());
			for (unsigned i = 0; i < init->getNumOperands(); i++){
				Constant* c = init->getOperand(i);
				DBG(dbgs() << "Constant " << i << "\n");
				DBG(c->getType()->dump());
				DBG(c->dump());
				initElems.push_back(c);
			}
			global_init->eraseFromParent();
		}

		// add TM initializer
		std::vector<Type*> StructTy_3_fields;
		StructTy_3_fields.push_back(IntegerType::get(M->getContext(), 32));
		FunctionType* FuncTy = FunctionType::get(Type::getVoidTy(M->getContext()), false);
		StructTy_3_fields.push_back(PointerType::get(FuncTy, 0));
		Type * pointerType = (Type *) PointerType::getUnqual(IntegerType::getInt8Ty(M->getContext()));
		StructTy_3_fields.push_back(pointerType);
		StructType* StructTy_3 = StructType::get(M->getContext(), StructTy_3_fields, false);

		ConstantInt* const_int32_12 = ConstantInt::get(M->getContext(), APInt(32, 65535));
		Function * func_stm_constructor = dyn_cast<Function> (M->getOrInsertFunction(HYDRARUNTIME_INIT, FuncTy));
		Constant* nullArg = Constant::getNullValue(pointerType);

		std::vector<Constant*> const_struct_11_fields;
		const_struct_11_fields.push_back(const_int32_12);
		const_struct_11_fields.push_back(func_stm_constructor);
		const_struct_11_fields.push_back(nullArg);
		Constant* const_struct_11 = ConstantStruct::get(StructTy_3, const_struct_11_fields);

		initElems.push_back(const_struct_11);

		// create new global init
		ArrayType* ArrayTy_1 = ArrayType::get(StructTy_3, initElems.size());
		Constant* newInit = ConstantArray::get(ArrayTy_1, initElems);
		global_init = new GlobalVariable(*M, ArrayTy_1, false,
					GlobalValue::AppendingLinkage, newInit,
					"llvm.global_ctors");
	}

	/* Destory Runtime */
	{
		GlobalVariable * global_destory = M->getGlobalVariable("llvm.global_dtors");

		std::vector<Constant*> destoryElems;
		if (global_destory) {
			// get other initializers
			ConstantArray* init = cast<ConstantArray>(global_destory->getInitializer());
			for (unsigned i = 0; i < init->getNumOperands(); i++){
				Constant* c = init->getOperand(i);
				DBG(dbgs() << "Constant " << i << "\n");
				DBG(c->getType()->dump());
				DBG(c->dump());
				destoryElems.push_back(c);
			}
			global_destory->eraseFromParent();
		}

		// add TM initializer
		std::vector<Type*> StructTy_3_fields;
		StructTy_3_fields.push_back(IntegerType::get(M->getContext(), 32));
		FunctionType* FuncTy = FunctionType::get(Type::getVoidTy(M->getContext()), false);
		StructTy_3_fields.push_back(PointerType::get(FuncTy, 0));
		Type * pointerType = (Type *) PointerType::getUnqual(IntegerType::getInt8Ty(M->getContext()));
		StructTy_3_fields.push_back(pointerType);
		StructType* StructTy_3 = StructType::get(M->getContext(), StructTy_3_fields, false);

		ConstantInt* const_int32_12 = ConstantInt::get(M->getContext(), APInt(32, 65535));
		Function * func_stm_constructor = dyn_cast<Function> (M->getOrInsertFunction(HYDRARUNTIME_DESTORY, FuncTy));
		Constant* nullArg = Constant::getNullValue(pointerType);

		std::vector<Constant*> const_struct_11_fields;
		const_struct_11_fields.push_back(const_int32_12);
		const_struct_11_fields.push_back(func_stm_constructor);
		const_struct_11_fields.push_back(nullArg);
		Constant* const_struct_11 = ConstantStruct::get(StructTy_3, const_struct_11_fields);

		destoryElems.push_back(const_struct_11);

		// create new global init
		ArrayType* ArrayTy_1 = ArrayType::get(StructTy_3, destoryElems.size());
		Constant* newInit = ConstantArray::get(ArrayTy_1, destoryElems);
		global_destory = new GlobalVariable(*M, ArrayTy_1, false,
					GlobalValue::AppendingLinkage, newInit,
					"llvm.global_dtors");
	}


	/* Register APIs */
	initLoadStore(NULL, "");
	/* Replace Loads & Stores */
	ilist<BasicBlock>::iterator sItr = F.getBasicBlockList().begin();
	sItr++;	//skip Entry block that loads parameters
	BasicBlock &functionEntry = F.getEntryBlock();

	SmallVector<BasicBlock*, 8> transactionalBlocks;
	SmallVector<BasicBlock*, 8> exitsBlocks;
	DBG(dbgs() << "Transactional Blocks:\n");
	for(ilist<BasicBlock>::iterator eItr = F.getBasicBlockList().end(); sItr!=eItr; sItr++){
		BasicBlock* bb = &*sItr;
		if(dyn_cast<ReturnInst>(bb->getTerminator())){
			exitsBlocks.push_back(bb);
			DBG(dbgs() << "\tblock " << bb->getName() << " 'exit'\n");
		}
		for (BasicBlock::iterator i = bb->begin(), ie = bb->end(), inext; i != ie; i = inext) {
			// Get next instruction before modifying the current instruction (it's an ilist...)
			inext = i;
			inext++;
	        if (Instruction *ins = dyn_cast<Instruction>(&*i)) {
	            if(ins->getMetadata("lerna")){
	                continue;
	            }
	        }
	        bool transactional = false;
			if(dyn_cast<LoadInst>(i) || dyn_cast<StoreInst>(i)){
				transactional = true;
			}else if(CallInst* CI = dyn_cast<CallInst>(i)){
				 if (const Function *F = CI->getCalledFunction()){
					 if(F->getName().find("lernaruntime_", 0) != StringRef::npos){	// e.g. tx_increment calls
							transactional = true;
					 }
				 }
			}
			if(transactional){
				DBG(dbgs() << "\tblock " << bb->getName() << " 'transactional' [" << *i << "]\n");
				transactionalBlocks.push_back(bb);
				break;
			}
		}
	}
	DominatorTree *DT =  &getAnalysis<DominatorTreeWrapperPass>().getDomTree();
	/*
	 * We disable the TxStart moving at current time, because it is implemented using setjmp-longjmp.
	 * using a non-volatile local in conjunction with with a setjmp-longjmp control transfer is undefined
	 * local variables that are not 'volatile' may be stored in registers rather than on the stack.
	 * setjmp/longjmp is implemented by saving the registers (including stack and code pointers etc) when
	 * first passed, and restoring it when jumping. So setjmp saves ONLY variables at the stack NOT ones
	 * stored at REGISTERS! As we don't know if variables are in STACK or REGISTERS, we can't guarantee the
	 * result!
	 * Options to solve this is to:
	 * 	- use Exception Handling of LLVM instead setjmp-longjmp
	 * 	- Extract Tx into another inner function and execute it there	(Current Implementation)
	 * 	- define all local variables as volatile
	 */
	bool no_dominator = false;
	while(transactionalBlocks.size() > 1){
		BasicBlock* bb1 = transactionalBlocks.pop_back_val();
		BasicBlock* bb2 = transactionalBlocks.pop_back_val();
		BasicBlock* dominator = DT->findNearestCommonDominator(bb1, bb2);
		DBG(dbgs() << "\t" << bb1->getName() << " + " << bb2->getName() << " = " <<  (dominator ? dominator->getName() : "none") << "\n");
		if(!dominator){
			no_dominator = true;
			break;
		}
		transactionalBlocks.push_back(dominator);
	}
	BasicBlock* common_dominator = no_dominator ? &functionEntry : transactionalBlocks.pop_back_val();
	DBG(dbgs() << "Common Dominator: " << common_dominator->getName() << "\n");
	CallInst *callStartTx;
	{
		Instruction* startInst = common_dominator->getFirstNonPHI();
	    AllocaInst* ai = new AllocaInst(
	    		ArrayType::get(Type::getInt8Ty(M->getContext()), 64), "",	//FIXME architecture dependent
	    		startInst);
	    Value* aiv = new BitCastInst(ai, Type::getInt8PtrTy(M->getContext()), "", startInst);
		Type* arr2[] = {
				Type::getInt8PtrTy(M->getContext())
		};
		ArrayRef<Type *> TySetJmpArgs(arr2);
		FunctionType *TyFuncSetJmpTx = FunctionType::get(Type::getInt32Ty(M->getContext()), TySetJmpArgs, false);
	    Constant* sjf = M->getOrInsertFunction("setjmp", TyFuncSetJmpTx);
	    ArrayRef<Value*> ParamJmp(aiv);
	    CallInst *abort_flags = CallInst::Create(sjf, ParamJmp, "", startInst);

		Type* arr3[] = {
				Type::getInt8PtrTy(M->getContext()),
				IntegerType::getInt32Ty(M->getContext()),
		};
		ArrayRef<Type *> TyFuncLernaInitArgs(arr3);
		Type* type_voidptr = PointerType::getUnqual(IntegerType::get(M->getContext(), 8));
		FunctionType *TyFuncLernaStartTx = FunctionType::get(type_voidptr, TyFuncLernaInitArgs, false);
		Constant* FuncLernaStartTx = M->getOrInsertFunction(HYDRARUNTIME_START_TX, TyFuncLernaStartTx);
		Value* arr1[] = {
				aiv,
				abort_flags
		};
		ArrayRef<Value*> ParamFuncLernaInit(arr1);
		callStartTx = CallInst::Create(FuncLernaStartTx, ParamFuncLernaInit, "", startInst);
	}

	FunctionType *TyFuncLernaInit = FunctionType::get(Type::getVoidTy(M->getContext()), false);
	Constant *FuncEndTx = M->getOrInsertFunction(HYDRARUNTIME_END_TX, TyFuncLernaInit);
	for (unsigned int i=0; i<exitsBlocks.size(); i++) {
		if(DT->dominates(common_dominator, exitsBlocks[i])){
			DBG(dbgs() << "Transactional Exit: " << exitsBlocks[i]->getName() << "\n");
			CallInst::Create(FuncEndTx,	"", exitsBlocks[i]->getFirstNonPHI());
		}
	}

	sItr = F.getBasicBlockList().begin();
	sItr++;	//skip Entry block that loads parameters
	for(ilist<BasicBlock>::iterator eItr = F.getBasicBlockList().end(); sItr!=eItr; sItr++){
		BasicBlock* bb = &*sItr;
		DBG(dbgs() << "\tblock " + bb->getName() + "\n");
		for (BasicBlock::iterator i = bb->begin(), ie = bb->end(), inext; i != ie; i = inext) {
			// Get next instruction before modifying the current instruction (it's an ilist...)
			inext = i;
			inext++;

	        if (Instruction *ins = dyn_cast<Instruction>(&*i)) {
	            if(ins->getMetadata("lerna")){
	                DBG(dbgs() << "\t\t" << *ins << "  [metadata=" << cast<MDString>(ins->getMetadata("lerna")->getOperand(0))->getString() << "]\n");
	                continue;
	            }
	        }

			if(LoadInst *LI = dyn_cast<LoadInst>(i)){
				DBG(dbgs() << "\t\t[" << l << "] Load: " << *LI << "\n");
				replaceLoad(LI, callStartTx, NULL);
			}else if(StoreInst *SI = dyn_cast<StoreInst>(i)){
				DBG(dbgs() << "\t\t[" << s << "] Store: " << *SI << "\n");
				replaceStore(SI, callStartTx, NULL);
			}
		}
	}

	if(!no_dominator){
		SmallVector<BasicBlock*, 8> txBlocksTree;
		DT->getDescendants(common_dominator, txBlocksTree);
		std::vector<BasicBlock*> toExtract;
		DBG(dbgs() << "Extract Tree:\n");
		for(unsigned int i=0; i<txBlocksTree.size(); i++){
			DBG(dbgs() << "\t" << txBlocksTree[i]->getName() << "\n");
			toExtract.push_back(txBlocksTree[i]);
		}
		// Extract the body of the if.
		Function* extractedFunction = CodeExtractor(toExtract).extractCodeRegion();
		if(!extractedFunction){
			DBG(dbgs() << "Not Extractable Region!\n");
		}
	}

	return false;
}

/**
 * Get or create load/store functions.
 * Does not create special functions to load/store from/to memory regions.
 */
void LernaTransactifier::initLoadStore(std::vector<Type*>* additionalArgs, const char* nameModifier)
{
    Type* type_voidptr = PointerType::getUnqual(IntegerType::get(M->getContext(), 8));
    const unsigned loadstore_funcs_count = 7;

    static const char* loads[loadstore_funcs_count] =  {
    		HYDRARUNTIME_READ_TX_8bit,
    		HYDRARUNTIME_READ_TX_16bit,
    		HYDRARUNTIME_READ_TX_32bit,
    		HYDRARUNTIME_READ_TX_64bit,
    		HYDRARUNTIME_READ_TX_FLOAT,
    		HYDRARUNTIME_READ_TX_DOUBLE,
    		HYDRARUNTIME_READ_TX_Ptr
    };
    static const char* stores[loadstore_funcs_count] =  {
    		HYDRARUNTIME_WRITE_TX_8bit,
    		HYDRARUNTIME_WRITE_TX_16bit,
    		HYDRARUNTIME_WRITE_TX_32bit,
    		HYDRARUNTIME_WRITE_TX_64bit,
    		HYDRARUNTIME_WRITE_TX_FLOAT,
    		HYDRARUNTIME_WRITE_TX_DOUBLE,
    		HYDRARUNTIME_WRITE_TX_Ptr
    };

    func_load = new Function*[loadstore_funcs_count];
    func_store = new Function*[loadstore_funcs_count];
    for (unsigned i = 0; i < loadstore_funcs_count; i++) {
        Type* ty;
        switch(i){
        	case 0: ty = IntegerType::get(M->getContext(), 8); break;
        	case 1: ty = IntegerType::get(M->getContext(), 16); break;
        	case 2: ty = IntegerType::get(M->getContext(), 32); break;
        	case 3: ty = IntegerType::get(M->getContext(), 64); break;
        	case 4: ty = Type::getFloatTy(M->getContext()); break;
        	case 5: ty = Type::getDoubleTy(M->getContext()); break;
        	case 6: ty = Type::getInt8PtrTy(M->getContext()); break;
        }
        PointerType* ptrty;
        switch(i){
			case 0:
			case 1:
			case 2:
			case 3:
			case 6:
				ptrty = PointerType::getUnqual(ty); break;
			case 4: ptrty = Type::getFloatPtrTy(M->getContext()); break;
			case 5: ptrty = Type::getDoublePtrTy(M->getContext()); break;
        }
        std::vector<Type*> fargs;
        fargs.push_back(ptrty);
        fargs.push_back(type_voidptr); // txn desc
        if (additionalArgs) fargs.insert(fargs.end(), additionalArgs->begin(), additionalArgs->end());
        FunctionType* ft = FunctionType::get(ty, fargs, false);
        std::string name = loads[i];
		name += nameModifier;
        func_load[i] =  dyn_cast<Function> (M->getOrInsertFunction(name.c_str(), ft));
        func_load[i]->addFnAttr(Attribute::AlwaysInline);
        dbgs() << func_load[i] << " " << name << "( ";
        for(unsigned int j=0; j<fargs.size(); j++){
            dbgs() << (j == 0  ? "" : ", ") << *fargs[j];
        }
        dbgs() << " )\n";
    }
    for (unsigned i = 0; i < loadstore_funcs_count; i++) {
        Type* ty;
        switch(i){
        	case 0: ty = IntegerType::get(M->getContext(), 8); break;
        	case 1: ty = IntegerType::get(M->getContext(), 16); break;
        	case 2: ty = IntegerType::get(M->getContext(), 32); break;
        	case 3: ty = IntegerType::get(M->getContext(), 64); break;
        	case 4: ty = Type::getFloatTy(M->getContext()); break;
        	case 5: ty = Type::getDoubleTy(M->getContext()); break;
        	case 6: ty = Type::getInt8PtrTy(M->getContext()); break;
        }
        PointerType* ptrty;
        switch(i){
			case 0:
			case 1:
			case 2:
			case 3:
			case 6:
				ptrty = PointerType::getUnqual(ty); break;
			case 4: ptrty = Type::getFloatPtrTy(M->getContext()); break;
			case 5: ptrty = Type::getDoublePtrTy(M->getContext()); break;
        }
        std::vector<Type*> fargs;
        fargs.push_back(ptrty);
        fargs.push_back(ty);
        fargs.push_back(type_voidptr); // txn desc
        if (additionalArgs) fargs.insert(fargs.end(),
                additionalArgs->begin(), additionalArgs->end());
        FunctionType* ft = FunctionType::get(Type::getVoidTy(
               M->getContext()), fargs, false);
        std::string name = stores[i];
		name += nameModifier;
        func_store[i] =  dyn_cast<Function> (M->getOrInsertFunction(name.c_str(), ft));
        func_store[i]->addFnAttr(Attribute::AlwaysInline);
        dbgs() << func_store[i] << " " << name << "( ";
        for(unsigned int j=0; j<fargs.size(); j++){
            dbgs() << (j == 0  ? "" : ", ") << *fargs[j];
        }
        dbgs() << " )\n";
    }
}

/**
 * Returns the load/store function for a certain type, or zero if there
 * is no suitable function.
 */
Function* LernaTransactifier::getIntLoadStoreFunction(Function** functions, const Type* type, bool aligned)
{
    Function* f = 0;
    if(type->isFloatTy())
    	return functions[4];
    if(type->isDoubleTy())
        return functions[5];
    if (type->isIntegerTy()) {
		const IntegerType *ti = cast<IntegerType>(type);
		switch (ti->getBitWidth()) {
			case 8: f = functions[0]; break;
			case 16: f = functions[1]; break;
			case 32: f = functions[2]; break;
			case 64: f = functions[3]; break;
		}
    }
    if(type->isPointerTy()){
    	return functions[6];
    }
    return f;
}



/**
 * Returns the type that the provided type must be casted to before a
 * load or store.
 */
const Type* LernaTransactifier::getLoadStoreConversion(Module* M, const DataLayout* TD, Type* type)
{
    const Type* to = type;
    if (isa<PointerType> (type)) {
        // convert to integer
        if (TD->getPointerSize() == 32)
            to = IntegerType::get(M->getContext(), 32);
        else if (TD->getPointerSize() == 64)
            to = IntegerType::get(M->getContext(), 64);
        else{
//        	LOG(dbgs() << "Unsupported pointer size " << TD->getPointerSize() << " " << *type << "\n");
//        	assert(0 && "unsupported pointer size");
        	to = PointerType::getUnqual(Type::getInt8PtrTy(M->getContext()));
        }
    }
    else if (type->isFloatingPointTy()) {
        // convert to integer
        if (type == Type::getFloatTy(M->getContext()))
            to = Type::getFloatTy(M->getContext());
        else if (type == Type::getDoubleTy(M->getContext()))
            to = Type::getDoubleTy(M->getContext());
    }
    else if (type->isIntegerTy() && cast<IntegerType> (type)->getBitWidth() < 8) {
        // use a bigger integer type if we are allowed to store more
        // TODO: use this for bigger integers too?
        to = IntegerType::get(M->getContext(), TD->getTypeStoreSizeInBits(type));
    }
    return to;
}

/**
 * Replaces a single load instruction with a matching call to a load function.
 */
Value* LernaTransactifier::replaceLoad(LoadInst* li, Value* tx, std::vector<Value*>* additionalArgs)
{
    Type* castType = li->getType();
    const Type* type = getLoadStoreConversion(M, D, castType);
    DBG(dbgs() << "Load Type (" << *((Type*)castType) << ") converted (" << *((Type*)type) << ")\n");
    if (isa<VectorType>(type)) {
        // TODO implement load/store for vectors
        assert(0 && "vector types are not yet supported");
    }
    Function* f = getIntLoadStoreFunction(func_load, type, false);
    if (type->isIntegerTy()) {
        unsigned bitwidth = cast<IntegerType> (type)->getBitWidth();
        if (f) {
            if (type == castType && bitwidth < 8) {
                assert(bitwidth == 1 && "type is smaller than i8, but is not i1");
                type = IntegerType::get(M->getContext(), 8);
            }
            if (type != castType) {
            	DBG(dbgs() << "Need Cast from (" << *((Type*)castType) << ") to (" << *((Type*)type) << ")\n");
            	Type* type2 = const_cast<Type*>(type);
                Value* from = new BitCastInst(li->getPointerOperand(), PointerType::getUnqual(type2), "", li);
                std::vector<Value*> v;
                v.push_back(from);
                DBG(dbgs() << "Args Types (" << *(from->getType()) << ")\n");
                v.push_back(tx);
                if (additionalArgs) v.insert(v.end(), additionalArgs->begin(), additionalArgs->end());
                DBG(dbgs() << "\t\t\tCalling " << f->getName() << "\n");
                Instruction* ci = CallInst::Create(f, v, li->getName(), li);
//                tif->setCallProps(cast<CallInst> (ci));
                // pointers must be casted using inttoptr/ptrtoint
                // if we need less bits for integer types, we truncate
                if (isa<PointerType> (castType))
                    ci = new IntToPtrInst(ci, castType);
                else if (isa<IntegerType> (castType))
                    ci = new TruncInst(ci, castType);
                else ci = new BitCastInst(ci, castType);
                ReplaceInstWithInst(li, ci);
                return ci;
            }
            else {
                std::vector<Value*> v;
                v.push_back(li->getPointerOperand());
                DBG(dbgs() << "Args Types (" << *(li->getPointerOperand()->getType()) << ")\n");
                v.push_back(tx);
                if (additionalArgs) v.insert(v.end(), additionalArgs->begin(), additionalArgs->end());
                DBG(dbgs() << "\t\t\tCalling " << f->getName() << "\n");
                CallInst* ci = CallInst::Create(f, v, li->getName());
//                tif->setCallProps(ci);
                ReplaceInstWithInst(li, ci);
                return ci;
            }
        }else{
        	errs() << "ERROR: no TX API found for this type [bits=" << bitwidth << "]\n";
        }
	} else if (type->isFloatingPointTy()) {
	    Function* f = getIntLoadStoreFunction(func_load, type, false);
		if (f) {
			std::vector<Value*> v;
			v.push_back(li->getPointerOperand());
            DBG(dbgs() << "Args Types (" << *(li->getPointerOperand()->getType()) << ")\n");
			v.push_back(tx);
			if (additionalArgs)
				v.insert(v.end(), additionalArgs->begin(), additionalArgs->end());
			CallInst* ci = CallInst::Create(f, v, li->getName());
			DBG(dbgs() << "\t\t\tCalling " << f->getName() << "\n");
			ReplaceInstWithInst(li, ci);
			return ci;
        }else{
        	errs() << "ERROR: no TX API found for this type\n";
        }
	} else{
		DBG(dbgs() << "Need Cast from (" << *((Type*)castType) << ") to (" << *((Type*)type) << ")\n");
		Type* type2 = const_cast<Type*>(type);
		Value* from = new BitCastInst(li->getPointerOperand(), type2, "", li);
		std::vector<Value*> v;
		v.push_back(from);
		DBG(dbgs() << "Args Types (" << *(from->getType()) << ")\n");
		v.push_back(tx);
		if (additionalArgs) v.insert(v.end(), additionalArgs->begin(), additionalArgs->end());
		DBG(dbgs() << "\t\t\tCalling " << f->getName() << "\n");
		Instruction* ci = CallInst::Create(f, v, li->getName(), li);
		ci = new BitCastInst(ci, castType);
		ReplaceInstWithInst(li, ci);
		return ci;
	}
    errs() << "ERROR: load to unsupported type ";
    type->print(errs());
    errs() << "\n";
    return li; // nothing changed
}



/**
 * Replaces a single store instruction with a matching call to a store function.
 */
void LernaTransactifier::replaceStore(StoreInst* si, Value* tx, std::vector<Value*>* additionalArgs)
{
    // operands: store(value, ptr)
    Value* value = si->getOperand(0);
    Type* castType = value->getType();

    const Type* type = getLoadStoreConversion(M, D, castType);
    DBG(dbgs() << "Store Type (" << *((Type*)castType) << ") converted (" << *((Type*)type) << ")\n");

    if (isa<VectorType>(type)) {
        // TODO implement load/store for vectors
        assert(0 && "vector types are not yet supported");
    }

    Function* f = getIntLoadStoreFunction(func_store, type, false);
    if (type->isIntegerTy()) {
        unsigned bitwidth = cast<IntegerType> (type)->getBitWidth();
        if (f) {
            if (type == castType && bitwidth < 8) {
                assert(bitwidth == 1 && "type is smaller than i8, but is not i1");
                type = IntegerType::get(M->getContext(), 8);
            }
            Value *target = si->getPointerOperand();
            if (type != castType) {
            	DBG(dbgs() << "Need Cast from (" << *((Type*)castType) << ") to (" << *((Type*)type) << ")\n");
            	Type* type2 = const_cast<Type*>(type);
                target = new BitCastInst(target, PointerType::getUnqual(type2),"", si);
                // if the function takes more bits than available, we zero extend to the larger type
                if (isa<PointerType> (castType))
                    value = new PtrToIntInst(value, type2, "", si);
                else if (isa<IntegerType> (castType))
                    value = new ZExtInst(value, type2, "", si);
                else value = new BitCastInst(value, type2, "", si);
            }
            std::vector<Value*> v;
            v.push_back(target);
            v.push_back(value);
            DBG(dbgs() << "Args Types (" << *(target->getType()) << ") (" << *(value->getType()) << ")\n");
            v.push_back(tx);
            if (additionalArgs) v.insert(v.end(),
                    additionalArgs->begin(), additionalArgs->end());
            DBG(dbgs() << "\t\t\tCalling " << f->getName() << " " << v.size() << "\n");
            CallInst* ci = CallInst::Create(f, v, si->getName());
//            tif->setCallProps(ci);
            ReplaceInstWithInst(si, ci);
            return;
        }
    } else if (type->isFloatingPointTy()) {
    	    Function* f = getIntLoadStoreFunction(func_store, type, false);
    	    DBG(dbgs() << "\t\t\tCalling " << f->getName() << "\n");
    		if (f) {
    			std::vector<Value*> v;
				v.push_back(si->getPointerOperand());
				v.push_back(value);
				v.push_back(tx);
				if (additionalArgs) v.insert(v.end(),
						additionalArgs->begin(), additionalArgs->end());
				CallInst* ci = CallInst::Create(f, v,
						si->getName());
	//            tif->setCallProps(ci);
				ReplaceInstWithInst(si, ci);
				return;
    		}
	} else{
		Value *target = si->getPointerOperand();

		DBG(dbgs() << "Ptr (" << *((Type*)target->getType()) << ") Val (" << *((Type*)value->getType()) << ")\n");

		if (type != target->getType()) {
			DBG(dbgs() << "Need Cast from (" << *((Type*)castType) << ") to (" << *((Type*)type) << ")\n");
			Type* type2 = const_cast<Type*>(type);
			target = new BitCastInst(target, type2,"", si);
			// if the function takes more bits than available, we zero extend to the larger type
			value = new BitCastInst(value, Type::getInt8PtrTy(M->getContext()), "", si);
		}
		std::vector<Value*> v;
		v.push_back(target);
		v.push_back(value);
		DBG(dbgs() << "Args Types (" << *(target->getType()) << ") (" << *(value->getType()) << ")\n");
		v.push_back(tx);
		if (additionalArgs) v.insert(v.end(),
				additionalArgs->begin(), additionalArgs->end());
		DBG(dbgs() << "\t\t\tCalling " << f->getName() << " " << v.size() << "\n");
		CallInst* ci = CallInst::Create(f, v, si->getName());
//            tif->setCallProps(ci);
		ReplaceInstWithInst(si, ci);
		return;
	}
    errs() << "ERROR: store to unsupported type ";
    type->print(errs());
    errs() << "\n";
}

