
#define DEBUG_TYPE "lerna_annotation"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Pass.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Lerna/Lerna.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include <iostream>
#include <fstream>
using namespace std;
using namespace llvm;
using namespace lerna;

STATISTIC(LernaAnnotationCounter, "Annotated Functions");
STATISTIC(LernaAnnotationSafeLoadsCounter, "Annotated Safe Loads");
STATISTIC(LernaAnnotationSafeStoresCounter, "Annotated Safe Stores");

namespace lerna{
  // LernaAnnotation - The first implementation, without getAnalysisUsage.
  class LernaAnnotation : public FunctionPass, LernaPass {
   public:
    static char ID; // Pass identification, replacement for typeid
    LernaAnnotation() : FunctionPass(ID) {}

    virtual bool runOnFunction(Function &F) {
    	if(lookupDictionary(&F, "SAFE.txt")){
    		LernaAnnotationCounter++;
			unsigned int reads = 0, writes = 0;
			for(Function::iterator i=F.begin(); i!=F.end(); ++i){
				for(BasicBlock::iterator j=i->begin();j!=i->end();++j){
					if(LoadInst *LI = dyn_cast<LoadInst>(j)){
						addMetaData(LI, "safe");
						reads++;
						LernaAnnotationSafeLoadsCounter++;
					}else if(StoreInst *SI = dyn_cast<StoreInst>(j)){
						addMetaData(SI, "safe");
						writes++;
						LernaAnnotationSafeStoresCounter++;
					}

				}
			}
			if(reads>0){
				LOG(dbgs() << "Manual Safe Function: " + F.getName() << " : " << reads << " reads, " << writes << " writes.\n");
				return true;
			}
		}
		return false;
    }
  };
}

char LernaAnnotation::ID = 0;
static RegisterPass<LernaAnnotation>
Y("annotation", "LernaVM: Use user annotation for transformation enhancements");

char &lerna::LernaAnnotationID = LernaAnnotation::ID;
FunctionPass *createLernaAnnotationPass() { return new LernaAnnotation(); }
